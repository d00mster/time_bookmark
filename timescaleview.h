#ifndef TIMESCALEVIEW_H
#define TIMESCALEVIEW_H

#include <memory>

#include <QEvent>
#include <QFutureWatcher>
#include <QGraphicsView>
#include <QMap>
#include <QMouseEvent>

#include <bookmarkmodel.h>

class QGraphicsScene;
class Bookmark;
class BookmarkItem;
class Cluster;

class TimeScaleView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit TimeScaleView(QWidget *parent = nullptr);

    void setModel(BookmarkModel *);

protected:
    void paintEvent(QPaintEvent *event);

private slots:
    void onTimeOut();
    void addItemsToScene();

private:
    void resizeEvent(QResizeEvent *event);
    double pixelDistance(const Bookmark &b1, const Bookmark &b2, double scale) const;
    void drawScale(QPainter&);
    void drawBookmarks();
    std::vector<BookmarkItem*> clusterize(const std::set<Bookmark> &bookmarks, double scale);

    /*bool eventFilter(QObject* pObj, QEvent* pEvent)
    {
        // We need to check for both types of mouse release, because it can vary on which type happens when resizing.
        if ((pEvent->type() == QEvent::MouseButtonRelease) || (pEvent->type() == QEvent::NonClientAreaMouseButtonRelease)) {
            QMouseEvent* pMouseEvent = dynamic_cast<QMouseEvent*>(pEvent);
            if ((pMouseEvent->button() == Qt::MouseButton::LeftButton) && isResizing_) {
                qDebug() << "Gotcha!";
                isResizing_ = false; // reset user resizing flag
            }
        }
        return QObject::eventFilter(pObj, pEvent); // pass it on without eating it
    }*/

private:
    QGraphicsScene* scene_;
    QTimer* timer_;
    BookmarkModel* model_;
    QFutureWatcher<std::vector<BookmarkItem*>> watcher_;
    bool isResizing_;
};

#endif // TIMESCALEVIEW_H
