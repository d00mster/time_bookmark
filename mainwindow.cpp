#include "mainwindow.h"

#include <random>

#include <QDebug>

#include <QInputDialog>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QVBoxLayout>

#include "ui_mainwindow.h"
#include "bookmark.h"
#include "timescaleview.h"

using Ms = std::chrono::milliseconds;

constexpr int maxDuration = 1000 * 60 * 60 * 3;
constexpr int msInHour = 1000 * 60 * 60;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    model_(new BookmarkModel(this))
{
    ui->setupUi(this);
    ui->timeScaleView->setModel(model_);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::generateBookmarks(int count)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis1(0, 23 * msInHour);
    std::uniform_int_distribution<> dis2(1000, maxDuration);

    for (int i = 0; i < count; ++i) {
        auto start = dis1(gen);
        auto duration = dis2(gen);
        while (start + duration > 24 * msInHour)
            duration = dis2(gen);
        auto newBookmark = Bookmark(Ms(start), Ms(duration));
        model_->addBookmark(newBookmark);
    }
}

void MainWindow::on_genBookmarksButton_clicked()
{
    model_->clear();
    bool ok;
    auto count = QInputDialog::getInt(this, tr("Generate Bookmarks"),
        tr("Enter bookmarks amount"), 10, 1, 100000000, 10, &ok);
    if (ok) {
        generateBookmarks(count);
    }
}
