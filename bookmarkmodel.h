#ifndef BOOKMARKMODEL_H
#define BOOKMARKMODEL_H

#include <set>

#include <QObject>

#include <bookmark.h>

class BookmarkModel : public QObject
{
public:
    explicit BookmarkModel(QObject* parent = nullptr) : QObject(parent) {}

    void addBookmark(const Bookmark& bm);
    void clear();

    const std::set<Bookmark> &bookmarks() const;

private:
    std::set<Bookmark> bookmarks_;
};

#endif // BOOKMARKMODEL_H
