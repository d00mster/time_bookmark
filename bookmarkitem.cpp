#include "bookmarkitem.h"

#include <QDebug>
#include <QPainter>

#include "bookmark.h"

BookmarkItem::BookmarkItem(const Bookmark *bookmark, int index, double scale,
                           QGraphicsItem *parent) :
    QGraphicsRectItem(parent),
    text_(bookmark->text()),
    start_(bookmark->startTime().count()),
    scale_(scale)
{
    setAcceptHoverEvents(true);
    setToolTip(text_);
    end_ = start_ + bookmark->duration().count();

    setOpacity(0.8);
    setPos(start_ * scale, 0);
    setRect(0, 0, end_ * scale, 20);
    setPen(QColor(Qt::blue));
    setBrush(QColor(Qt::blue));

    bookmarks_.push_back(bookmark);
}

const QString& BookmarkItem::text() const
{
    return text_;
}

void BookmarkItem::merge(const Bookmark* bookmark)
{
    bookmarks_.push_back(bookmark);

    auto toolTip = buildClusterInfo();
    setToolTip(toolTip);

    text_ = QString::number(bookmarks_.size());
    auto end = (bookmark->startTime() + bookmark->duration()).count();
    if (end_ < end)
        end_ = end;

    setRect(0, 0, end_ * scale_, 20);
    setPen(QColor(Qt::green));
    setBrush(QColor(Qt::green));
}

QColor BookmarkItem::penColor() const
{
    return (bookmarks_.size() > 1) ? QColor(Qt::green) : QColor(Qt::blue);
}

QColor BookmarkItem::brushColor() const
{
    return (bookmarks_.size() > 1) ? QColor(Qt::green) : QColor(Qt::blue);
}

void BookmarkItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
//    qDebug() << "paint" << start_ * scale_ << end_ * scale_;

    auto point = mapFromScene(end_ * scale_, 20);
    setRect(0, 0, point.x(), point.y());
    QGraphicsRectItem::paint(painter, option, widget);

    QFontMetrics fm(painter->font());
    auto elidedText = fm.elidedText(text(), Qt::ElideRight, rect().width());

    painter->setPen(Qt::white);
    painter->drawText(rect(), Qt::AlignLeft | Qt::AlignVCenter | Qt::TextWordWrap, elidedText);
}

QString BookmarkItem::buildClusterInfo()
{
    QString result;
    int i{1};
    for (auto& bookmark : bookmarks_) {
        result.append(bookmark->text() + '\n');
        if (++i > 15) {
            result.append(QString("+ %1 other bookmarks").arg(bookmarks_.size() - i));
            break;
        }
    }

    return result;
}
