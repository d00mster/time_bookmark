#include "bookmark.h"

Bookmark::Bookmark(std::chrono::milliseconds startTime,
                   std::chrono::milliseconds duration) :
    startTime_(startTime),
    duration_(duration)
{

}
const QString &Bookmark::text() const
{
    return text_;
}

void Bookmark::setText(const QString &text)
{
    text_ = text;
}

std::chrono::milliseconds Bookmark::startTime() const
{
    return startTime_;
}

std::chrono::milliseconds Bookmark::duration() const
{
    return duration_;
}
