#ifndef BOOKMARK_H
#define BOOKMARK_H

#include <chrono>

#include <QTime>
#include <QWidget>

class Bookmark
{
public:
    Bookmark() = default;
    Bookmark(std::chrono::milliseconds startTime,
             std::chrono::milliseconds duration);

    const QString& text() const;
    void setText(const QString&);
    std::chrono::milliseconds startTime() const;
    std::chrono::milliseconds duration() const;

    friend bool operator<(const Bookmark& lhs, const Bookmark& rhs) {
        return (lhs.startTime_ < rhs.startTime_);
    }

private:
    QString text_;
    std::chrono::milliseconds startTime_;
    std::chrono::milliseconds duration_;
};

#endif // BOOKMARK_H
