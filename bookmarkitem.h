#ifndef BOOKMARKITEM_H
#define BOOKMARKITEM_H

#include <memory>

#include <QDebug>
#include <QGraphicsRectItem>

class Bookmark;

class BookmarkItem : public QGraphicsRectItem
{
public:
    BookmarkItem(const Bookmark* bookmark, int index, double scale,
                 QGraphicsItem *parent = nullptr);

    const QString &text() const;

    void merge(const Bookmark *bookmark);

    QRectF boundingRect() const
    {
        qreal adjust = 0.5;
        return QRectF(-18 - adjust, -22 - adjust,
                      36 + adjust, 60 + adjust);
    }

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

private:
    QString buildClusterInfo();
    QColor penColor() const;
    QColor brushColor() const;

private:
    QString text_;
    int start_;
    int end_;
    double scale_;
    std::vector<const Bookmark*> bookmarks_;
};

#endif // BOOKMARKITEM_H
