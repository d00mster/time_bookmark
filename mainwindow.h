#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <timescaleview.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_genBookmarksButton_clicked();

private:
    void generateBookmarks(int count);

private:
    Ui::MainWindow *ui;
    BookmarkModel* model_;
};

#endif // MAINWINDOW_H
