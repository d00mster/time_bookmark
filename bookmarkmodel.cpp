#include "bookmarkmodel.h"

void BookmarkModel::addBookmark(const Bookmark &bookmark)
{
    bookmarks_.emplace(bookmark);
}

void BookmarkModel::clear()
{
    bookmarks_.clear();
}

const std::set<Bookmark> &BookmarkModel::bookmarks() const
{
    return bookmarks_;
}
