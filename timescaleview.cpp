#include "timescaleview.h"

#include <QDebug>
#include <QtConcurrent/QtConcurrent>
#include <QGraphicsScene>
#include <QPainter>
#include <QPaintEvent>
#include <QString>
#include <QTimer>

#include "bookmark.h"
#include "bookmarkitem.h"

TimeScaleView::TimeScaleView(QWidget *parent) :
    QGraphicsView(parent),
    scene_(new QGraphicsScene(this)),
    timer_(new QTimer(this)),
    isResizing_(false)
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setMinimumHeight(70);
    setMinimumWidth(200);
    setScene(scene_);
//    setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    qApp->installEventFilter(this);

    connect(&watcher_, SIGNAL(finished()), this, SLOT(addItemsToScene()));
    connect(timer_, SIGNAL(timeout()), this, SLOT(onTimeOut()));
    timer_->start(100);
}

void TimeScaleView::onTimeOut()
{
    drawBookmarks();
}

void TimeScaleView::addItemsToScene()
{
    auto items = watcher_.future().result();

    scene_->clear();
    for (auto& item : items) {
        scene_->addItem(item);
    }
    scene_->update();
}

void TimeScaleView::setModel(BookmarkModel* model)
{
    model_ = std::move(model);
}

void TimeScaleView::paintEvent(QPaintEvent *event)
{
    QPainter painter(viewport());
    painter.setRenderHint(QPainter::Antialiasing);

    drawScale(painter);

    QGraphicsView::paintEvent(event);
}

void TimeScaleView::resizeEvent(QResizeEvent *event)
{
//    QGraphicsView::resizeEvent(event);
    timer_->start(100);
    isResizing_ = true;
    scene_->setSceneRect(0, -35, width(), height());
}

double TimeScaleView::pixelDistance(const Bookmark &b1, const Bookmark &b2, double scale) const
{
    auto x1 = b1.startTime() * scale;
    auto x2 = b2.startTime() * scale;
    return (x2 - x1).count();
}

void TimeScaleView::drawScale(QPainter &painter)
{
    auto scale = width() / 24;

    painter.drawRect(painter.window());

    QFontMetrics fm(painter.font());
    for (int i = 0; i < 24; ++i) {
        painter.drawLine(i * scale, 0, i * scale, 20);
        auto text = QString::number(i) + "h";
        int width=fm.width(text)/2;
        painter.drawText(i * scale - width, 30, text);
    }
}

void TimeScaleView::drawBookmarks()
{
    if (model_ == nullptr)
        return;

    auto scale = width() / double(24 * 60 * 60 * 1000);

    auto future = QtConcurrent::run(this, &TimeScaleView::clusterize, model_->bookmarks(), scale);
    watcher_.setFuture(future);
}

std::vector<BookmarkItem*> TimeScaleView::clusterize(const std::set<Bookmark>& bookmarks, double scale) {
    std::vector<BookmarkItem*> result;
    int index{1};
    for (auto it = bookmarks.begin(), itPrev = bookmarks.begin(); it != bookmarks.end(); itPrev = it++, ++index) {
        auto& bookmark = *it;
        auto& prevBookmark = *itPrev;
        const_cast<Bookmark&>(bookmark).setText("Bookmark " + QString::number(index));
        auto pixels = pixelDistance(prevBookmark, bookmark, scale);
        if (pixels < 100 && it != bookmarks.begin()) {
            result.back()->merge(&bookmark);
        }
        else {
            result.push_back(new BookmarkItem(&bookmark, index, scale));
        }
    }
    return result;
}
